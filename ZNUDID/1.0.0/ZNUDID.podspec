#
#  Be sure to run `pod spec lint ZNUDID.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "ZNUDID" # 项目名称
  s.version      = "1.0.0"        # 版本号 与 你仓库的 标签号 对应
  s.license      = { :type => "MIT", :file => "LICENSE" }          # 开源证书
  s.summary      = "swift UDID 获取" # 项目简介
  s.homepage     = "https://gitlab.com/ZM_iOS/Component/ZNUDID" # 仓库的主页
  s.source       = { :git => "https://gitlab.com/ZM_iOS/Component/ZNUDID.git", :tag => "#{s.version}" }#你的仓库地址，不能用SSH地址
  s.source_files = 'Classes/*.{swift}'
  s.requires_arc = true # 是否启用ARC
  s.author             = { "ZengXianShu" => "zengxianshu0@163.com" }
  s.platform     = :ios, "9.0" #平台及支持的最低版本
  s.frameworks   = "AdSupport", "Security" #支持的框架
  s.swift_version = "4.0"
end

