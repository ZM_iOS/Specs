#
#  Be sure to run `pod spec lint ZNReader.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "ZNReader" # 项目名称
  s.version      = "0.3.6"        # 版本号 与 仓库的 标签号 对应
  s.license      = { :type => "MIT", :file => "LICENSE" }          # 开源证书
  s.summary      = "分为分页，翻页模式两部分" # 项目简介
  s.homepage     = "https://gitlab.com/ZM_iOS/Component/ZNReader" # 仓库的主页
  s.source       = { :git => "https://gitlab.com/ZM_iOS/Component/ZNReader.git", :tag => "#{s.version}" }
  s.source_files = 'Classes/*.{swift}','Classes/**/*.{swift}'
  s.requires_arc = true # 是否启用ARC
  s.author       = { "Guess" => "abc@xyz.com" } # 作者信息
  s.platform     = :ios, "9.0" #平台及支持的最低版本
  s.frameworks   = "UIKit", "CoreText" #支持的框架
  s.swift_version = "5.0" #所使用swift版本
end

