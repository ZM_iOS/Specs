#
#  Be sure to run `pod spec lint dyModel.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "DYNSUserDefaultsUtils"
  s.version      = "1.0.0"
  s.summary      = "NSUserDefaultsUtils"

  s.homepage     = "https://gitlab.com/ZM_iOS/Component/DYNSUserDefaultsUtils"
  s.license      = { :type => "MIT", :file => "LICENSE" } 
  

  s.author             = { "ZengXianShu" => "zengxianshu0@163.com" }
  s.platform     = :ios, "9.0"
  

  s.source       = { :git => "https://gitlab.com/ZM_iOS/Component/DYNSUserDefaultsUtils.git", :tag => "#{s.version}" }

  s.source_files  = "Pods/**/*.{h,m}"

  s.requires_arc = true


end
